/*
 *  JLib - Jacob's Library.
 *  Copyright (C) 2003, 2004  Juan Carlos Seijo P�rez
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 *  Juan Carlos Seijo P�rez
 *  jacob@mainreactor.net
 */

/** Definiciones para geometr�a 3D para OpenGL.
 * @file    JGLMesh.h
 * @author  Juan Carlos Seijo P�rez
 * @date    10/05/2004
 * @version 0.0.1 - 10/05/2004 - Primera versi�n.
 * @notes: Ficheros ASE - Curiosidad de MAX 5, pongo el ejemplo:
 * Supongamos una escena con tres bolas que comparten el mismo material multi/subobjeto
 * compuesto de tres submateriales de id 1, 2 y 3, respectivamente. Si las bolas
 * tienen id's 1, 2 y 3 respectivamente se asignar�n de esa forma los submateriales
 * a cada una de ellas. Sin embargo si alguna de ellas tiene un valor diferente (mayor),
 * el material que aplica MAX es el resto de dividir el id de la bola menos uno entre el n�mero
 * de submateriales del material y todo eso m�s uno. 
 * As� si las bolas tienen id's 1, 22 y 7, respectivamente,
 * - a la bola con MTLID 1 se le asigna el submaterial: ((1-1)%3) + 1 = (0%3) + 1 = 0 + 1 = 1
 * - a la bola con MTLID 23 se le asigna el submaterial: ((23-1)%3) + 1 = (21%3) + 1 = 0 + 1 = 1
 * - a la bola con MTLID 7 se le asigna el submaterial: ((8-1)%3) + 1 = (7%3) + 1 = 1 + 1 = 2
 * 
 * Notad que en el fichero ASE el MTLID es uno menos que en la escena ya.
 */

#ifndef _JGLMESH_INCLUDED
#define _JGLMESH_INCLUDED

#include <JLib/Util/JTypes.h>
#include <JLib/Util/JString.h>
#include <JLib/Util/JTextFile.h>
#include <JLib/Math/JVector.h>
#include <JLib/Graphics/JDrawable.h>
#include <JLib/Graphics/JASEFormat.h>
#include <JLib/Graphics/JGLTexture.h>

/** Color RGB. Todas las componentes van de 0.0f a 1.0f.
 */
struct JColor
{
  float r;                              /**< Componente roja. */
  float g;                              /**< Componente verde. */
  float b;                              /**< Componente azul. */
  float a;                              /**< Componente alfa. */
};

class JMaterial
{
 public:
	/** Crea un material vac�o.
	 */
	JMaterial(): texture(0)
	{}

  JGLTexture *texture;                  /**< Textura asociada. */
  s32 parentId;                         /**< Id del padre. */
  s32 id;                               /**< Id. */
  bool isSubMaterial;                   /**< Indica si es submaterial. */
  bool isMulti;                         /**< Indica si es Multi/sub-objeto. */
  s32 numSubMaterials;                  /**< N�meor de submateriales. */
  s8 mtlClass[32];                      /**< Tipo de material. */
  JColor ambient;                       /**< Color ambiente. */
  JColor diffuse;                       /**< Color difuso. */
  JColor specular;                      /**< Color especular. */
  float shine;                          /**< Cantidad de brillo. */
  float shineStrenght;                  /**< Intensidad de brillo. */
  float transparency;                   /**< Transparencia. */
  float selfIllum;                      /**< Iluminaci�n propia. */

	/** Devuelve la textura asociada a este material.
	 * @return La textura asociada a este material.
	 */
	JGLTexture * Texture()
	{
		return texture;
	}

	/** Libera los recursos asociados.
	 */
	void Destroy();

	/** Destruye el material y libera los recursos asociados.
	 */
	~JMaterial() {Destroy();}
};

typedef JVector JVertex;

/** Coordenada de textura.
 */
struct JTextureVertex
{
  float u;                              /**< Componente u. */
  float v;                              /**< Componente v. */
};

/** Cara.
 */
class JFace
{
 public:
  /** Crea el objeto. 
	 */
  JFace() : material(0) {}
  s32 v1;                               /**< V�rtice 1. */
  s32 v2;                               /**< V�rtice 2. */
  s32 v3;                               /**< V�rtice 3. */
  JMaterial *material;                  /**< material asociado. */
  JVector normal;                       /**< Vector normal. */
};

/** Cara con textura.
 */
struct JTextureFace
{
  s32 v1;                               /**< V�rtice 1. */
  s32 v2;                               /**< V�rtice 2. */
  s32 v3;                               /**< V�rtice 3. */
};

/** Clase de objeto 3D para OpenGL. Un objeto se compone de
 * v�rtices agrupados en caras, posiblemente texturizadas.
 * N�tese que los materiales se asignan a nivel de geometr�a.
 */
class JGLMeshObject
{
 public:
  JString name;                         /**< Nombre. */
  s32 numVertices;                      /**< N�mero de v�rtices. */
  s32 numTVertices;                     /**< N�mero de v�rtices de textura. */
  s32 numFaces;                         /**< N�mero de caras. */
  s32 numTFaces;                        /**< N�mero de caras de textura. */
  JVertex *vertices;                    /**< V�rtices. */
  JTextureVertex *tVertices;            /**< V�rtices de textura. */
  JVector *vertexNormals;               /**< Normales a los v�rtices. */
  JFace *faces;                         /**< Caras. */
  JTextureFace *tFaces;                 /**< Caras de textura. */

  /** Crea el objeto vac�o. 
	 */
  JGLMeshObject() : vertices(0), tVertices(0), vertexNormals(0), faces(0), tFaces(0)
	{}

	/** Devuelve el nombre de esta geometr�a.
	 * @return nombre de esta geometr�a.
	 */
	const JString & Name()
	{
		return name;
	}

	/** Establece el nombre de esta geometr�a.
	 * @param  newName nombre de esta geometr�a.
	 */
	void Name(const JString & newName)
	{
		name = newName;
	}

	/** Devuelve el n�mero de v�rtices de la geometr�a.
	 * @return N�mero de v�rtices de la geometr�a.
	 */
	s32 NumVertices()
	{
		return numVertices;
	}

	/** Devuelve el n�mero de v�rtices de textura de la geometr�a.
	 * @return El n�mero de v�rtices de textura de la geometr�a.
	 */
	s32 NumTVertices()
	{
		return numTVertices;
	}

	/** Devuelve el n�mero de caras de la geometr�a.
	 * @return El n�mero de caras de la geometr�a.
	 */
	s32 NumFaces()
	{
		return numFaces;
	}

	/** Devuelve el n�mero de caras de textura de la geometr�a.
	 * @return El n�mero de caras de textura de la geometr�a.
	 */
	s32 NumTFaces()
	{
		return numTFaces;
	}

	/** Devuelve los v�rtices de la geometr�a.
	 * @return V�rtices de la geometr�a.
	 */
	JVertex * Vertices()
	{
		return vertices;
	}

	/** Devuelve los v�rtices de textura de la geometr�a.
	 * @return V�rtices De Textura de la geometr�a.
	 */
	JTextureVertex * TVertices()
	{
		return tVertices;
	}

	/** Devuelve las normales a los v�rtices de la geometr�a.
	 * @return Las normales a los v�rtices de la geometr�a.
	 */
	JVector * VertexNormals()
	{
		return vertexNormals;
	}

	/** Devuelve las caras de la geometr�a.
	 * @return Las caras de la geometr�a.
	 */
	JFace * Faces()
	{
		return faces;
	}

	/** Devuelve las caras de textura de la geometr�a.
	 * @return Las caras de textura de la geometr�a.
	 */
	JTextureFace * TFaces()
	{
		return tFaces;
	}

	/** Libera los recursos asociados a este objeto.
	 */
	void Destroy();

	/** Destruye el objeto.
	 */
	virtual ~JGLMeshObject() {Destroy();}
};

/** Clase de geometr�a 3D para OpenGL. Consiste de una serie de objetos 3D
 * y sus correspondientes materiales.
 */
class JGLMesh : public JDrawable
{
 protected:
  s32 numMaterials;                     /**< N�mero de materiales. */
  s32 numObjects;                       /**< N�mero de objetos. */
  JMaterial *materials;                 /**< Materiales. */
  JGLMeshObject *objects;               /**< Objetos 3D de esta geometr�a. */
	
	JTextFile f;                          /**< Fichero de carga de objetos. */

	/** Carga los materiales de la geometr�a desde el fichero ASE abierto por LoadASE.
	 * @return <b>true</b> si se pudieron cargar, <b>false</b> si no.
	 */
	bool LoadASEMaterials();

	/** Carga los objetos de la geometr�a desde el fichero ASE abierto por LoadASE.
	 * @return <b>true</b> si se pudieron cargar, <b>false</b> si no.
	 */
	bool LoadASEObjects();

 public:
	/** Construye la geometr�a vac�a, lista para cargarse con Load().
	 */
	JGLMesh() : materials(0), objects(0)
	{}

	/** Devuelve el n�mero de materiales de la geometr�a.
	 * @return El n�mero de materiales de la geometr�a.
	 */
	s32 NumMaterials()
	{
		return numMaterials;
	}

	/** Devuelve el n�mero de objetos de esta geometr�a.
	 * @return El n�mero de objetos de esta geometr�a.
	 */
	s32 NumObjects()
	{
		return numObjects;
	}

	/** Devuelve los materiales de la geometr�a.
	 * @return Los materiales de la geometr�a.
	 */
	JMaterial * Materials()
	{
		return materials;
	}

	/** Devuelve los objetos de esta geometr�a.
	 * @return los objetos de esta geometr�a.
	 */
	JGLMeshObject * Objects()
	{
		return objects;
	}

	/** Dibuja la geometr�a.
	 */
	void Draw();

	/** Carga la geometr�a desde el fichero dado.
	 * Por ahora el fichero puede ser formato ASE �nicamente.
	 * @return <b>true</b> si se pudo cargar, <b>false</b> si no.
	 */
	bool LoadASE(const JString &filename);

	/** Carga la geometr�a desde el fichero J3D dado. Los ficheros J3D se generan a partir de Blender (www.blender.org)
   * con ayuda del script de exportaci�n 'JExporter'.
	 * @return <b>true</b> si se pudo cargar, <b>false</b> si no.
	 */
	bool LoadJ3D(const JString &filename);

	/** Libera los recursos asociados a este objeto.
	 */
	void Destroy();

	/** Destruye el objeto.
	 */
	virtual ~JGLMesh() {Destroy();}
};

#endif // _JGLMESH_INCLUDED
