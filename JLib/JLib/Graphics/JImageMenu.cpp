/*
 *  JLib - Jacob's Library.
 *  Copyright (C) 2003, 2004  Juan Carlos Seijo P�rez
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 *  Juan Carlos Seijo P�rez
 *  jacob@mainreactor.net
 */

/** Menu en pantalla compuesto de imag�nes.
 * @file    JImageMenu.cpp
 * @author  Juan Carlos Seijo P�rez
 * @date    28/04/2004
 * @version 0.0.1 - 28/04/2004 - Primera versi�n.
 * @version 0.0.2 - 25/09/2004 - Modificaci�n del m�todo 2D de Pos() para aceptar floats (quita libertad sino).
 */

#include <JLib/Graphics/JImageMenu.h>

bool JImageMenuEntry::Select()
{
	if (Action)
	{
		Action(data);
		return true;
	}
	
	return false;
}

JImageMenu::JImageMenu()
{
	curOption = options.NewIterator();
}

void JImageMenu::ApplyLayout(JTree<JImageMenuEntry *>::Iterator *it)
{
	// Si es distribuci�n libre, lo especificar�n desde fuera
	if (config.layout == JIMAGEMENU_FREE)
	{
		return;
	}

	s32 maxX = 0;
	s32 xOff = 0, yOff = 0;

	// Determina la anchura m�xima entre todas las opciones de esta rama
	do
	{
		maxX = maxX < it->Data()->Image()->Width() ? it->Data()->Image()->Width() : maxX;

		// Renderiza los hijos
		if (it->Child())
		{
			ApplyLayout(it);
		}
	} while (it->Next());

	it->FirstInBranch();

	// Aplica la distribuci�n de men�
	do
	{
		switch (config.layout)
		{
		case JIMAGEMENU_LEFT:
			it->Data()->Image()->Pos(0, yOff);
			it->Data()->HiImage()->Pos(0, yOff);
			yOff += it->Data()->Image()->Height();
			break;
				
		case JIMAGEMENU_RIGHT:
			xOff = -it->Data()->Image()->Width();
			it->Data()->Image()->Pos(xOff, yOff);
			it->Data()->HiImage()->Pos(xOff, yOff);
			yOff += it->Data()->Image()->Height();
			break;
				
		case JIMAGEMENU_CENTER:
			xOff = -(it->Data()->Image()->Width()/2);
			it->Data()->Image()->Pos(xOff, yOff);
			it->Data()->HiImage()->Pos(xOff, yOff);
			yOff += it->Data()->Image()->Height();
			break;
				
		case JIMAGEMENU_SAMELINE:
			it->Data()->Image()->Pos(xOff, 0);
			it->Data()->HiImage()->Pos(xOff, 0);
			xOff += it->Data()->Image()->Width();
			break;
				
		default:
			break;
		}
	} while (it->Next());

	it->Parent();
}

bool JImageMenu::Init(JImageMenuConfig &cfg)
{
	if (curOption == 0)
	{
		// No es una aplicaci�n v�lida o no se a�adieron opciones
		return false;
	}

	memcpy(&config, &cfg, sizeof(config));

	// Renderiza las opciones de men�
	curOption->Root();
	ApplyLayout(curOption);

	return true;
}

void JImageMenu::Pos(float x, float y)
{
	pos.x = x;
	pos.y = y;
	if (curOption)
	{
		// Renderiza las opciones de men�
		curOption->Root();
		ApplyLayout(curOption);
	}
}

void JImageMenu::Draw()
{
	JTree<JImageMenuEntry *>::Iterator *it = new JTree<JImageMenuEntry *>::Iterator(*curOption);
	JImage * img;

	it->FirstInBranch();

	do
	{
		// Si es la opci�n seleccionada muestra su imagen resaltada
		if (it->Data() == curOption->Data())
		{
			img = it->Data()->HiImage();
		}
		else
		{
			img = it->Data()->Image();
		}

		img->Draw((s32)(X() + img->X()), (s32)(Y() + img->Y()));

	} while (it->Next());
	
	delete it;
}

void JImageMenu::TrackKeyboard(SDL_keysym key)
{
	// Actualiza el estado seg�n el teclado
	if (config.trackKeyboard)
	{
		switch (key.sym)
		{
		case SDLK_TAB:
			// SHIFT + TAB
			if (JApp::App()->KeyMods() & KMOD_SHIFT)
			{
				// Opci�n anterior
				curOption->Prev();
			}
			// TAB
			else
			{
				// Opci�n siguiente
				curOption->Next();
			}
			break;

		case SDLK_UP:
		case SDLK_LEFT:
			// Opci�n anterior
			curOption->Prev();
			break;

		case SDLK_DOWN:
		case SDLK_RIGHT:
			// Opci�n siguiente
			curOption->Next();
			break;

		case SDLK_KP_ENTER:
		case SDLK_RETURN:
			// Ejecuta la acci�n asociada
			if (!curOption->Data()->Select() && config.autoEnter)
			{
				// Si no hay acci�n asociada y se especific� autoEnter, intenta descender
				// a la opci�n hija.
				curOption->Child();
			}
			break;
		
		case SDLK_ESCAPE:
			// Va a la opci�n padre.
			curOption->Parent();
			break;

		default:
			break;
		} // switch (key)
	} // Track keyboard
}

void JImageMenu::TrackMouse(s32 bt, s32 x, s32 y)
{
	// Actualiza el estado seg�n el rat�n
	if (config.trackMouse)
	{
		JTree<JImageMenuEntry *>::Iterator *it = new JTree<JImageMenuEntry *>::Iterator(*curOption);
		s32 mx, my;
		bool found = false;
		mx = JApp::App()->MouseX();
		my = JApp::App()->MouseY();

		it->FirstInBranch();

		// Comprueba si est� sobre alguna opci�n
		do
		{
			if (mx > it->Data()->Image()->X() + X() && 
					mx < it->Data()->Image()->X() + X() + it->Data()->Image()->Width() &&
					my > it->Data()->Image()->Y() + Y() && 
					my < it->Data()->Image()->Y() + Y() + it->Data()->Image()->Height())
			{
				// Est� dentro, hace que sea la opci�n resaltada
				found = true;

				// Borra el iterador actual
				delete(curOption);

				curOption = it;
			}
		} while (!found && it->Next());


		if (found)
		{
			// Si ahora est� pulsado, activa el flag de pulsaci�n
			if (bt & SDL_BUTTON_LEFT)
			{
				// Ejecuta la acci�n asociada
				if (!curOption->Data()->Select() && config.autoEnter)
				{
					// Si no hay acci�n asociada y se especific� autoEnter, intenta descender
					// a la opci�n hija.
					curOption->Child();
				}
			}
		}

		// Si encontr� una opci�n bajo el cursor el iterador sobre esa opci�n
		// pasa a ser el iterador de opci�n actual, no lo borra
		if (!found)
			delete it;
	} // Track mouse
}

