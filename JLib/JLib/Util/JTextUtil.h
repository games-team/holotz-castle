/*
 *  JLib - Jacob's Library.
 *  Copyright (C) 2003, 2004  Juan Carlos Seijo P�rez
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 *  Juan Carlos Seijo P�rez
 *  jacob@mainreactor.net
 */

///////////////////////////////////////////////////////////////////////////////
// @author: Juan Carlos Seijo P�rez
// @date: 24/05/2003
// @description: Funciones de manejo de texto de windows
///////////////////////////////////////////////////////////////////////////////

#ifndef _JTEXTUTIL_INCLUDED
#define _JTEXTUTIL_INCLUDED

#include <JLib/Util/JTypes.h>
#include <SDL.h>
//#include <JLib/Graphics/JText.h>

// Crea una superficie en formato RGBA de 32 bits con los caracteres en
// 16 filas y 16 columnas para el formato de fuente especificado.
// NOTA: Es responsabilidad del llamador liberar la memoria del buffer devuelto
// por esta funci�n.
//u32* GetFontSurface(JFontFormat format);

#endif  // _JTEXTUTIL_INCLUDED
